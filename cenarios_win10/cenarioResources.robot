*** Settings ***
Library        ImageHorizonLibrary    ${EXECDIR}/cenarios_win10/imagens          confidence=0.9
Resource       ../utils/utils.robot 


*** Variable ***
${Numero_1}                    Numero_1.png
${Numero_2}                    Numero_2.png
${Numero_3}                    Numero_3.png
${Numero_4}                    Numero_4.png
${Numero_5}                    Numero_5.png
${Numero_6}                    Numero_6.png    
${Numero_7}                    Numero_7.png
${Numero_8}                    Numero_8.png
${Numero_9}                    Numero_9.png
${Botao_soma}                  botao_soma.png 
${Botao_subtracao}             botao_subtracao.png
${Botao_multiplicacao}         botao_multiplicacao.png
${Botao_divisao}               botao_divisao.png
${Botao_igual}                 botao_igual.png 
${Resultado_soma}              resultado_soma.png
${Resultado_subtracao}         resultado_subtracao.png
${Resultado_multiplicacao}     resultado_multiplicacao.png
${Resultado_divisao}           resultado_divisao.png

*** Keywords ***
DADO que o usuario abra a calculadora
   Iniciar aplicacao
   Sleep  1

 QUANDO selecionar o valor 1
    Sleep    1 
    Click Image    ${Numero_1} 

QUANDO selecionar o valor 6
    Sleep    1 
    Click Image    ${Numero_6} 

QUANDO selecionar o valor 8
    Sleep    1 
    Click Image    ${Numero_8} 

E selecionar o botao +
    Sleep    1
    Click Image    ${Botao_soma}

E selecionar o botao -
    Sleep    1
    Click Image    ${Botao_subtracao}

E selecionar o botao x
    Sleep    1
    Click Image    ${Botao_multiplicacao}

E selecionar o botao :
    Sleep    1
    Click Image    ${Botao_divisao} 

E selecionar um valor 2
    Sleep    1
    Click Image    ${Numero_2} 

E selecionar um valor 3
    Sleep    1
    Click Image    ${Numero_3} 

E selecionar um valor 7
    Sleep    1
    Click Image    ${Numero_7} 

E selecionar um valor 9
    Sleep    1
    Click Image    ${Numero_9} 

E apertar o botao =
    Click Image    ${Botao_igual}

ENTAO o sistema deve apresentar o valor de soma.
    Wait For    ${Resultado_soma}

ENTAO o sistema deve apresentar um valor de subtracao.
    Wait For    ${Resultado_subtracao}

ENTAO o sistema deve apresentar um valor de multiplicacao.
    Wait For    ${Resultado_multiplicacao}

ENTAO o sistema deve apresentar um valor de divisao.
    Wait For    ${Resultado_divisao}
