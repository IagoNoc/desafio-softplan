*** Settings ***
Resource              ./cenarioResources.robot
Resource               ../utils/utils.robot

Test Teardown    Fechar aplicacao

*** Variables ***


*** Test Cases ***
Caso de Teste 01: Soma
    DADO que o usuario abra a calculadora
    QUANDO selecionar o valor 1
    E selecionar o botao +
    E selecionar um valor 2
    E apertar o botao =
    ENTAO o sistema deve apresentar o valor de soma.

Caso de Teste 02: subtracao
    DADO que o usuario abra a calculadora
    QUANDO selecionar o valor 1  
    E selecionar o botao -
    E selecionar um valor 9
    E apertar o botao =
    ENTAO o sistema deve apresentar um valor de subtracao.

Caso de Teste 03: multiplicacao
    DADO que o usuario abra a calculadora
    QUANDO selecionar o valor 8
    E selecionar o botao x
    E selecionar um valor 7
    E apertar o botao =
    ENTAO o sistema deve apresentar um valor de multiplicacao.

    
Caso de Teste 04: divisao
    DADO que o usuario abra a calculadora
    QUANDO selecionar o valor 6
    E selecionar o botao :
    E selecionar um valor 3
    E apertar o botao =
    ENTAO o sistema deve apresentar um valor de divisao.