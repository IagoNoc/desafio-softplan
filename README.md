# Desafio Softplan
Este Projeto foi desenvolvido em Python com Robot Framework
o projeto foi criado para rodar tanto na calculadora do windows 10 quanto no windows 11

# Pré-requisitos e configurações iniciais:

   ### NodeJS [versão LTS](https://nodejs.org/en/download/)

    - após instalar node, instalar allure através do npm utilizando o comando:
        - npm install -g allure-commandline --save-dev
# Baixando as dependências:
    - pip install robot-framework
    - pip install allure-robotframework
    - pip install Pillow --upgrade
    - pip install opencv-python
    - imagehorizonlibrary
    
# Rodando o projeto em modo headless:
    - robot --listener allure_robotframework ./my_robot_test
# Allure report:
    - allure serve ./output/allure
