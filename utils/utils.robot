*** Settings ***
Library        ImageHorizonLibrary    ${EXECDIR}/cenarios_win10/        confidence=0.9


*** Variable ***
${calculadora}              C:/Windows/System32/calc.exe
${Numero_10}                imagens/Numero_1.png
${Numero_20}                imagens/Numero_2.png
${Numero_30}                imagens/Numero_3.png
${Numero_40}                imagens/Numero_4.png
${Numero_50}                imagens/Numero_5.png
${Numero_60}                imagens/Numero_6.png    
${Numero_70}                imagens/Numero_7.png
${Numero_80}                imagens/Numero_8.png
${Numero_90}                imagens/Numero_9.png
${Fechar}                   Botao_fechar.png



*** Keywords ***
Iniciar aplicacao
    Launch Application    ${calculadora}

Fechar aplicacao
    Press Combination    Key.Alt    Key.F4